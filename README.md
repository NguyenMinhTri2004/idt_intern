# Các bước có thể sử dụng tính năng:

1. Tải file hoặc clone repo về máy.
2. Tạo 1 instance mới của class MyBigNumber bằng lệnh new MyBigNumber(), truyền vào hai tham số với kiểu dữ liệu là string, đây sẽ là hai số được cộng với nhau.
3. Gọi hàm (method) sum trên instance đã tạo.
4. Hàm sẽ trả về 1 string là kết quả cộng của hai tham số được truyền vào.
5. Đối với testing. Truy cập vào UnitTest/index.html - > Open With Live Server để tiến hành chạy chương trình test
6. Ở phần main.test.js là những testcase cơ bản : Tốc độ chương trình, kiểu dữ liệu trả về....
