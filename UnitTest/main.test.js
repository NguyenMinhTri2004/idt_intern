import {MyBigNumber} from "../add2Num.js";

const expect = chai.expect

const numString = new MyBigNumber();

describe("test MyBigNumber", function ()  {

    this.timeout(500)

    it("should take less than 500ms", function (done) {
        
        numString.sum('10000','200');
        done() 
    })

    
    it("should equal 12000", function () {
        
        const result = numString.sum('10000','200');
        expect(result).to.equal(10000)
    })

    it("should be string", function () {
        
        const result = numString.sum('10000','200');
        expect(result).to.be.a('string')
    })
})